﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalAI : MonoBehaviour
{
    public enum tipe { Grass, Meat, Fish, Fruit }
    public tipe eat;
    Vector3 newPos;
    NavMeshAgent navMeshAgent;
    Transform target;

    Queue<string> tasks;

    List<Transform> waterResources;
    List<Transform> bedResources;
    List<Transform> foodResources;

    private float timer;
    [SerializeField] float time = 4;

    //stats
    [SerializeField] float hungry;
    [SerializeField] float stamina;
    [SerializeField] float thirsty;

    GameObject[] numResources;

    int state;

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        tasks = new Queue<string>();
        target = null;

        waterResources = new List<Transform>();
        bedResources = new List<Transform>();
        foodResources = new List<Transform>();

        hungry = Random.Range(70, 100);
        stamina = Random.Range(70, 100);
        thirsty = Random.Range(70, 100);

        ResetList();
    }

    void Update()
    {
        timer -= Time.deltaTime;

        ManageStats();  // Diferentes cantidades para cada animal / individuo
        Movement();
        ManageTasks();
    }

    private void ManageStats()
    {
        if (timer < 0)
        {
            timer = time;
            hungry -= 3;
            stamina -= 1;
            thirsty -= 4;
        }

        if (stamina < 30 && !tasks.Contains("Bed"))
        {
            tasks.Enqueue("Bed");
        }
        if (thirsty < 50 && !tasks.Contains("Water"))
        {
            tasks.Enqueue("Water");
        }
        if (hungry < 40 && !tasks.Contains("Food"))
        {
            tasks.Enqueue("Food");
        }
    }

    private void ManageTasks()  ///Perfect
    {
        if (tasks.Count > 0)
        {
            switch (tasks.Peek())
            {
                case "Bed":
                    SearchFor(stamina);
                    break;

                case "Food":
                    SearchFor(hungry);
                    break;

                case "Water":
                    SearchFor(thirsty);
                    break;
            }
        }
    }

    private void SearchFor(float resource)
    {
        switch (state)
        {
            case 0:
                SetObjetive(tasks.Peek());
                state = 1;
                break;

            case 1:
                if (resource > 80)
                    state = 2;
                break;

            case 2:
                tasks.Dequeue();
                state = 0;
                break;
        }
    }

    private void Movement()
    {
        if (navMeshAgent != null)
        {
            if (target != null)
                newPos = target.transform.position;
            else
                newPos = transform.position;

            navMeshAgent.SetDestination(newPos);
        }
    }

    private void SetObjetive(string name)
    {
        switch (name)
        {
            case "Food":
                target = GetClosestResource(foodResources, this.transform);
                break;
            case "Water":
                target = GetClosestResource(waterResources, this.transform);
                break;
            case "Bed":
                target = GetClosestResource(bedResources, this.transform);
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Hacer que los recursos den x cantidad de comida, agua, etc. No terminas la tarea hasta que no llegas a x cantidad del recurso
        string name = other.gameObject.tag;
        if (name == "Bed")
        {
            stamina = 100;
        }
        if (name == "Food")
        {
            hungry = 100;
        }
        if (name == "Water")
        {
            thirsty = 100;
        }

        Destroy(other.gameObject);
        StartCoroutine(DoWait());
    }

    IEnumerator DoWait()
    {
        yield return new WaitForSeconds(1);
        ResetList();
    }

    private void ResetList() //Optimizar esta puta mierda
    {
        if (foodResources.Count > 0)
            foodResources.Clear();
        if (bedResources.Count > 0)
            bedResources.Clear();
        if (waterResources.Count > 0)
            waterResources.Clear();

        numResources = GameObject.FindGameObjectsWithTag("Food");
        for (int i = 0; i < numResources.Length; i++)
        {
            foodResources.Add(numResources[i].transform);
        }

        numResources = GameObject.FindGameObjectsWithTag("Bed");
        for (int i = 0; i < numResources.Length; i++)
        {
            bedResources.Add(numResources[i].transform);
        }

        numResources = GameObject.FindGameObjectsWithTag("Water");
        for (int i = 0; i < numResources.Length; i++)
        {
            waterResources.Add(numResources[i].transform);
        }
    }

    //TODO use mesh distance instead of normal distance
    Transform GetClosestResource(List<Transform> resources, Transform fromThis)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = fromThis.position;
        foreach (Transform potentialTarget in resources)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }
        return bestTarget;
    }
}