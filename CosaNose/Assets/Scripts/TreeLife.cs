﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeLife : MonoBehaviour
{
    TimeManager tm;
    private int hour;
    private int multiplier = 1;
    private float multiplierAmount;
    public GameObject fruit;
    public GameObject[] fruitInstance;
    Vector3 newScale;
    private int numFruits;
    // Start is called before the first frame update
    void Start()
    {
        tm = FindObjectOfType<TimeManager>();
        hour = tm.GetHour();
        newScale = transform.localScale;
        multiplierAmount = newScale.x;

        numFruits = Random.Range(10, 20);
        fruitInstance = new GameObject[numFruits];
    }

    // Update is called once per frame
    void Update()
    {
        if(hour < tm.GetHour())
        {
            hour++;
            if(multiplier < 3)
            { 
                multiplier++;
                newScale = new Vector3(multiplierAmount * multiplier, multiplierAmount * multiplier, multiplierAmount * multiplier);
            }
            switch(hour)//hacer mas estados
            {
                case 3:
                    CreateFruits();
                    break;
                case 4:
                    GrowFruits();
                    break;
                case 5:
                    DropFruits();
                    break;
                case 6:
                    newScale = Vector3.zero;
                    break;
                case 7:
                    Destroy(gameObject);
                    break;
            }
        }
        transform.localScale = Vector3.Lerp(transform.localScale, newScale, Time.deltaTime * 2);
    }

    private void DropFruits()
    {
        for (int i = 0; i < fruitInstance.Length; i++)
        {
            fruitInstance[i].GetComponent<Rigidbody>().useGravity = true;
            fruitInstance[i].GetComponent<SphereCollider>().enabled = true;
        }
    }

    private void GrowFruits()
    {
        for (int i = 0; i < fruitInstance.Length; i++)
        {
            fruitInstance[i].transform.localScale *= 2f;
        }
    }

    private void CreateFruits()
    {
        for (int i = 0; i < fruitInstance.Length; i++)
        {
            Vector3 v = new Vector3();
            float angle = Random.Range(1, 360);
            v.x = transform.position.x + (1 * Mathf.Cos(angle));
            v.y = transform.position.y + Random.Range(2, 4);
            v.z = transform.position.z + (1 * Mathf.Sin(angle));
            fruitInstance[i] = Instantiate(fruit, v, Quaternion.identity);
            fruitInstance[i].GetComponent<Rigidbody>().useGravity = false;
            fruitInstance[i].GetComponent<SphereCollider>().enabled = false;
            fruitInstance[i].transform.localScale *= 0.5f;
        }
    }
}
