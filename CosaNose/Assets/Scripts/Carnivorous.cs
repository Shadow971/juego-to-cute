﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Carnivorous : MonoBehaviour
{
    public enum tipeEnum { Lobo}
    [Tooltip("Tipo de animal")] public tipeEnum tipe;
    public enum animEnum{ Oveja }
    [Tooltip("Tipo de comida a cazar")] public animEnum animEat;
    public enum foodEnum { Grass, Meat, Fish, Fruit }
    [Tooltip("Tipo de comida del terreno")] public foodEnum foodEat;
    public enum stateEnum { Idle, Sleeping, Eating, Drinking, Movement, Hunting, Playing, Searching}
    [Tooltip("Estado en el que se encuentra el animal")] public stateEnum state;

    [Tooltip("Cantidad de hambre ")] [SerializeField] int hungry;
    [Tooltip("Cantidad de sed  ")] [SerializeField] int thirsty;
    [Tooltip("Cantidad de sueño ")] [SerializeField] int energy;
    [Tooltip("Variacion entre animales")] [SerializeField] float variation;

    float timer = 4;
    int switchState = 1;

    //Bed
    private Transform bedPlace;
    SphereCollider sc;
   
    NavMeshAgent navMeshAgent;
    Vector3 target;

    Queue<string> task;
     
    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        sc = GetComponent<SphereCollider>();
        sc.enabled = false;
        state = stateEnum.Idle;

        hungry = 100;
        thirsty = 100;
        energy = 100;
        switchState = 1;


        task = new Queue<string>();

        variation = Random.Range(0.5f, 2f);
        //todo set bedPlace;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            timer = 4;
            UpdateVariables();
        }
        ManageTask();
    }

    private void Movement()
    {
        if (navMeshAgent != null)
        {
            navMeshAgent.SetDestination(target);
        }
    }

    private void UpdateVariables()
    {
        switch (state)
        {
            case stateEnum.Idle:
                hungry -= 1 ;
                thirsty -= 1;
                energy -= 1;
                break;

            case stateEnum.Sleeping:
                hungry -= 1;
                thirsty -= 1;
                energy += 4;
                break;

            case stateEnum.Movement:
                hungry -= 2;
                thirsty -= 2;
                energy -= 2;
                break;

            case stateEnum.Hunting:
                hungry -= 3;
                thirsty -= 3;
                energy -= 3;
                break;

            case stateEnum.Playing:
                hungry += 2;
                thirsty += 2;
                energy -=1;
                break;

            case stateEnum.Searching:
                hungry += 3;
                thirsty += 3;
                energy += 3;
                break;
        }
        
    }

    private void ManageTask()
    {
        if(hungry <= 40 * variation && !task.Contains("Hunt"))
        {
           // task.Enqueue("Hunt");
        }
       
        if(energy <= 40 * variation && !task.Contains("Bed"))
        {
            task.Enqueue("Bed");
        }

        if(thirsty <= 40 * variation && !task.Contains("Water"))
        {
            //task.Enqueue("Water");
        }
        ManageQuest();
    }

    private void ManageQuest()
    {
        if(task.Count > 0)
        {
            switch (task.Peek())
            {
                case "Bed":
                    SearchNearest();
                    break;
                case "Resource":
                    SearchNearest();
                    break;
                case "Water":
                    SearchNearest();
                    break;
            }
        }
    }

    private void SearchNearest()
    {
        switch (switchState)
        {
            case 1 :

                sc.enabled = true;
                sc.radius += Time.deltaTime;

                break;

            case 2 :
                if (Vector3.Distance(transform.position, target) < 1)
                {
                    state = stateEnum.Sleeping;
                    if (energy > 80) state = stateEnum.Idle;
                }
                break;
        }
    }

    private void SearchFood()
    {

    }

    private void SearchWater()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == task.Peek())
        {
            switch (task.Peek())
            {
                case "Bed":
                    target = other.transform.position;
                    break;

                case "Resource":

                    break;

                case "Water":

                    break;
            }

            switchState++;
            sc.radius = 0f;
            sc.enabled = false;
        }
    }

    // todo checkModel (Food Script)
    // todo OntriggerEnter (Eat && Drink)
}
