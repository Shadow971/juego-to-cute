﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid
{
    private int width;
    private int height;
    private int[,] gridArray;
    private float cellSize;
    public Grid(int width, int height, float size)
    {
        this.width = width;
        this.height = height;
        cellSize = size;
        gridArray = new int[width, height];

        for (int i = 0; i < gridArray.GetLength(0); i++)
        {
            for (int j = 0; j < gridArray.GetLength(1); j++)
            {
                Debug.DrawLine(GetWorldPosition(i, j), GetWorldPosition(i, j + 1), Color.white, 100f);
                Debug.DrawLine(GetWorldPosition(i, j), GetWorldPosition(i + 1, j), Color.white, 100f);
            }
        }
        Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
        Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);
    }

    private void GetXZ(Vector3 worldPos, out int x, out int z)
    {
        x = Mathf.FloorToInt(worldPos.x / cellSize);
        z = Mathf.FloorToInt(worldPos.y / cellSize);
    }

    public GridCoord GetGridPos(Vector3 worldPos)
    {
        int x, z;
        GetXZ(worldPos, out x, out z);
        GridCoord coord = new GridCoord(x, z);
        return coord;
    }
    public Vector3 GetWorldPosition(int x, int z)
    {
        return new Vector3(x, 0, z) * cellSize;
    }
}

public class GridCoord
{
    public int x;
    public int z;
    public GridCoord(int x, int z)
    {
        this.x = x;
        this.z = z;
    }

}
