﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CameraController : MonoBehaviour
{
    [SerializeField] Transform camTransform;
    [SerializeField] Transform rotateTransform;
    private MoveObjects script;

    public Vector3 zoomAmount;
    private Vector3 newZoom;
    [SerializeField] float maxZoom = 100;
    [SerializeField] float minZoom = 5;

    [SerializeField] float screenOffset = 20f;

    [SerializeField] float normalSpeed;
    [SerializeField] float fastSpeed;
    private float speed;
    [SerializeField] float time;

    private Vector3 newPos;
    private Quaternion newRotation;
    [SerializeField] float rotationAmount;
    private Vector3 newPivotRot;

    private Vector3 dragStartPos;
    private Vector3 dragCurrentPos;

    private Vector3 rotateStartPos;
    private Vector3 rotateCurrentPos;

    private int rotIterator = 0;

    private GameObject targetTransform = null;
    private bool moving;

    //Options
    [SerializeField] bool moveWithMouse = true;
    [SerializeField] bool snapRotation = true;
    [SerializeField] int snapAngle = 0;
    private readonly int[] angleAmount = { 30, 45, 60, 75, 90 };

    void Start()
    {
        script = FindObjectOfType<MoveObjects>();
        newPivotRot = rotateTransform.eulerAngles;
        newPos = transform.position;
        newRotation = transform.rotation;
    }
    private void Awake()
    {
        newZoom = new Vector3(0, maxZoom / 4, -maxZoom / 4);
    }

    void Update()
    {
        float dt = Time.unscaledDeltaTime;
        Vector3 mousePos = Input.mousePosition;

        ObjectInfo objScript = FindObjectOfType<ObjectInfo>();
        //targetTransform = objScript.GetTarget();
        
        Rotation(dt);
        Zoom(dt);
        ChangeSpeed();
        DragMovement(mousePos);
        Movement(dt, mousePos);
        if (targetTransform != null)
        {
            newPos = targetTransform.transform.position;
        }
        transform.position = Vector3.Lerp(transform.position, newPos, dt * time);
    }

    private void DragMovement(Vector3 mp)
    {
        if (Input.GetMouseButton(1) || Input.GetMouseButton(2)) Cursor.visible = false;
        else Cursor.visible = true;

        if (Input.GetMouseButtonDown(1))
        {
            Plane plane = new Plane(Vector3.up, Vector3.zero);
            Ray ray = Camera.main.ScreenPointToRay(mp);

            if (plane.Raycast(ray, out float entry))
                dragStartPos = ray.GetPoint(entry);
        }

        if (Input.GetMouseButton(1))
        {
            Plane plane = new Plane(Vector3.up, Vector3.zero);
            Ray ray = Camera.main.ScreenPointToRay(mp);

            if (plane.Raycast(ray, out float entry))
            {
                dragCurrentPos = ray.GetPoint(entry);
                newPos = transform.position + dragStartPos - dragCurrentPos;
            }
        }

        if (Input.GetMouseButtonDown(2))
        {
            rotateStartPos = mp;
        }

        if (Input.GetMouseButton(2))
        {
            rotateCurrentPos = mp;
            Vector3 difference = rotateStartPos - rotateCurrentPos;
            difference.x = Mathf.Clamp(difference.x, -100, 100);
            rotateStartPos = rotateCurrentPos;

            newPivotRot.x += difference.y / 5f;
            newPivotRot.x = Mathf.Clamp(newPivotRot.x, -40f, 45f);

            newRotation *= Quaternion.Euler(Vector3.up * (-difference.x / 5f));
        }
    }

    private void Movement(float dt, Vector3 mp)
    {
        float xInput = Input.GetAxisRaw("Horizontal");
        float zInput = Input.GetAxisRaw("Vertical");

        if (xInput != 0)
        {
            moving = true;
            newPos += transform.right * speed * xInput;
        }
        if (zInput != 0)
        {
            moving = true;
            newPos += transform.forward * speed * zInput;
        }
        
        if(zInput == 0 && xInput == 0)
        {
            moving = false;
        }

        if (moveWithMouse)
        {
            MouseMovement(mp);
        }        
    }

    public bool GetIfMoving() { return moving; }
    private void MouseMovement(Vector3 mp)
    {
        if (mp.x < screenOffset)
        {       
            newPos -= transform.right * speed * CalculateDistanceBorders(mp, screenOffset - mp.x);
        }
        if (mp.x > Screen.width - screenOffset)
        {          
            newPos += transform.right * speed * CalculateDistanceBorders(mp, (mp.x - (Screen.width - screenOffset)));
        }
        if (mp.y < screenOffset)
        {         
            newPos -= transform.forward * speed * CalculateDistanceBorders(mp, screenOffset - mp.y);
        }
        if (mp.y > Screen.height - screenOffset)
        {
            newPos += transform.forward * speed * CalculateDistanceBorders(mp, mp.y - (Screen.height - screenOffset));
        }
    }

    private float CalculateDistanceBorders(Vector3 mp, float op)
    {
        float distance = op;
        distance = Mathf.Clamp(distance, 0, screenOffset);
        distance /= screenOffset;
        return distance;
    }

    private void Zoom(float dt)
    {
        float scrollInput = Input.mouseScrollDelta.y;

        if (scrollInput != 0 && !script.GetIfRotating())
        {
            newZoom += zoomAmount * scrollInput;
            newZoom = new Vector3(newZoom.x, Mathf.Clamp(newZoom.y, minZoom, maxZoom),
            Mathf.Clamp(newZoom.z, -maxZoom, -minZoom));
        }
        camTransform.localPosition = Vector3.Lerp(camTransform.localPosition, newZoom, dt * time);
    }

    private void Rotation(float dt)
    {
        if (snapRotation)
        {
            int numRotations = (360 / angleAmount[snapAngle]) - 1;
            if (Input.GetKeyDown(KeyCode.Q))
            {
                rotIterator++;
                if (rotIterator > numRotations) rotIterator = 0;
                newRotation = Quaternion.Euler(0, angleAmount[snapAngle] * rotIterator, 0);
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                rotIterator--;
                if (rotIterator < 0) rotIterator = numRotations;
                newRotation = Quaternion.Euler(0, angleAmount[snapAngle] * rotIterator, 0);
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.Q))
            {
                newRotation *= Quaternion.Euler(Vector3.up * rotationAmount);
            }
            if (Input.GetKey(KeyCode.E))
            {
                newRotation *= Quaternion.Euler(Vector3.up * -rotationAmount);
            }
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, dt * time);
        rotateTransform.localRotation = Quaternion.Lerp(rotateTransform.localRotation, Quaternion.Euler(newPivotRot), dt * time);
    }

    private void ChangeSpeed()
    {
        if (Input.GetKey(KeyCode.LeftShift))
            speed = fastSpeed;
        else
            speed = normalSpeed;
    }
}
