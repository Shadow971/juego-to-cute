﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class GenerateName
{
    private string[] names;
    private string name;
    
    public GenerateName()
    {
        string path = Application.dataPath + "/Files/Names.txt";
        names = File.ReadAllLines(path);
        int i = Random.Range(0, names.Length);
        name = names[i];
    }
    public string GetName() { return name; }
}
