﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosaPaProbar : MonoBehaviour
{
    [SerializeField] new string name;
    [SerializeField] int numero;
    // Start is called before the first frame update
    void Awake()
    {
        GenerateName n = new GenerateName();
        name = n.GetName();
    }

    public void ChangeName(string name)
    {
        this.name = name;
        Debug.Log(name);
    }
}
