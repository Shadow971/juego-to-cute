﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceFruitOnTree : MonoBehaviour
{
    [SerializeField] Transform target;
    Ray ray;
    private RaycastHit hit;
    void Start()
    {
        transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
        ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit))
        {
            transform.position = hit.point;
        }
    }
}
