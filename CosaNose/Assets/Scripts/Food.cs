﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Food : MonoBehaviour
{
    public enum tipeEnum { Grass, Meat, Fish, Fruit }

    [Tooltip("NO Grass")] public tipeEnum tipe;
    float amount;
    public bool spawn = false;
    public bool used = false;
    [Tooltip("Prefab of tree")]public GameObject tree;
    
    // Start is called before the first frame update
    void Start()
    {
        amount = Random.Range(30, 100);
        transform.localScale *= (amount / 50);

        ChekModel();

    }

    private void Update()
    {
        amount -= Time.deltaTime/10;
        if(amount <= 0 && tipe == tipeEnum.Fruit)
        {
            Instantiate(tree);
            Destroy(this);
        }
    }

    private void ChekModel()
    {
        switch (tipe)
        {
            case tipeEnum.Grass:
                spawn = true;
                Destroy(this.transform.GetChild(1).gameObject);
                Destroy(this.transform.GetChild(2).gameObject);
                Destroy(this.transform.GetChild(3).gameObject);
                break;
            case tipeEnum.Meat:
                Destroy(this.transform.GetChild(0).gameObject);
                Destroy(this.transform.GetChild(2).gameObject);
                Destroy(this.transform.GetChild(3).gameObject);
                break;
            case tipeEnum.Fish:
                Destroy(this.transform.GetChild(0).gameObject);
                Destroy(this.transform.GetChild(1).gameObject);
                Destroy(this.transform.GetChild(3).gameObject);
                break;
            case tipeEnum.Fruit:
                Destroy(this.transform.GetChild(0).gameObject);
                Destroy(this.transform.GetChild(1).gameObject);
                Destroy(this.transform.GetChild(2).gameObject);
                break;
        }
        int nChild = this.transform.GetChild(0).transform.childCount;
        int child = Random.Range(0, nChild);
        for (int i = 0; i < nChild; i++)
        {
            if (i != child)
            {
                Destroy(this.transform.GetChild(2).gameObject.transform.GetChild(i).gameObject);
            }
        }
    }
}
