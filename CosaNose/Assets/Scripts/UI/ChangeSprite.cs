﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSprite : MonoBehaviour
{
    [SerializeField] Sprite[] sprites;
    Button button;
    int i = 0;

    private void Start()
    {
        button = GetComponent<Button>();
        button.image.sprite = sprites[i];
    }

    public void ChangeImage()
    {
        i++;
        if (i > sprites.Length - 1) i = 0;
        button.image.sprite = sprites[i];
    }
}
