﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveObjects : MonoBehaviour
{
    private GameObject target;
    public GameObject indicator;
    private GameObject indicatorInstance;
    [SerializeField] Slider sl;

    private Ray ray;
    private RaycastHit hit;
    bool placing;

    private Vector3 newRotation;
    private float[] snapAngles = { 15, 30, 45, 60, 75, 90 };
    private float snapAngle;
    private bool rotating;
    private bool canPlace = true;
    [SerializeField] bool rotateWithSnap = true;

    public void SetSnapAngle()
    {
        snapAngle = snapAngles[(int)sl.value];
    }

    private void Start()
    {
        snapAngle = snapAngles[0];
    }
    void Update()
    {
        Vector3 v = Input.mousePosition;
        v.z = 1;
        ray = Camera.main.ScreenPointToRay(v);

        if (placing)
        {
            Rotation();
            MoveTarget();
        }

        if (Input.GetMouseButtonDown(0) && canPlace)
        {
            if (Physics.Raycast(ray, out hit))//pillar collider del target para hacer el indicador
            {
                if (placing) placing = false;
                if (hit.transform.gameObject.CompareTag("Movable"))
                {
                    target = hit.transform.gameObject;
                }
            }
        }
    }

    public bool GetIfPlacing() { return placing; }
    public GameObject GetTargetPlacing() { return target; }
    public void AcceptSelect()
    {
        placing = !placing;
        indicatorInstance = Instantiate(indicator, hit.point, Quaternion.identity);
        target.GetComponent<BoxCollider>().enabled = false;
        target.GetComponent<Rigidbody>().useGravity = false;
        newRotation = target.transform.eulerAngles;
    }

    public void CreateObject(GameObject go)
    {
        if(!placing)
        {
            target = Instantiate(go);
            if (Physics.Raycast(ray, out hit))
            {
                target.transform.position = hit.point;
            }
            indicatorInstance = Instantiate(indicator, hit.point, Quaternion.identity);
            target.GetComponent<BoxCollider>().enabled = false;
            target.GetComponent<Rigidbody>().useGravity = false;
            newRotation = target.transform.eulerAngles;
            placing = true;
        }
    }

    private void MoveTarget()
    {
        if (Input.GetMouseButtonDown(0) && canPlace)
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (target != null)
                {
                    Destroy(indicatorInstance.gameObject);
                    target.GetComponent<BoxCollider>().enabled = true;
                    target.GetComponent<Rigidbody>().useGravity = true;
                }
            }
        }

        if(Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.Escape))
        {
            Destroy(target.gameObject);
            Destroy(indicatorInstance.gameObject);
            placing = false;
            target = null;
        }

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 currentPos = (hit.point);
            if (indicatorInstance != null)
                indicatorInstance.transform.position = currentPos;

            if(hit.transform.gameObject.tag == "CantPlace")
            {
                canPlace = false;
            }
            else
            {
                canPlace = true;
            }

            currentPos.y += 1.5f;
            target.transform.position = Vector3.Lerp(target.transform.position, currentPos, Time.unscaledDeltaTime * 5);
        }
    }


    private void OnGUI()
    {
        if (placing)
        {
            GUI.Label(new Rect(Input.mousePosition.x + 30, -Input.mousePosition.y + Screen.height - 20, 1000, 100),
                "-Position: " + target.transform.position.x.ToString("N") + ", " + target.transform.position.z.ToString("N")
                + "\n-Rotation: " + target.transform.eulerAngles.y);
        }
    }

    private void Rotation()
    {
        float mScroll = Input.mouseScrollDelta.y;
        if (Input.GetKey(KeyCode.Z))
        {
            rotating = true;
            if (mScroll != 0)
            {
                float angle;
                if (rotateWithSnap) angle = snapAngle * mScroll;
                else angle = mScroll;

                newRotation = (Vector3.up * angle);
                target.transform.eulerAngles += newRotation;
            }
        }
        else rotating = false;
    }

    public bool GetIfRotating() { return rotating; }
}
