﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpandChildTabs : MonoBehaviour
{
    Vector2[] childOrigins = new Vector2[10];
    bool expand;
    void Start()
    {
        SetInitialPositions();
    }

    private void SetInitialPositions()
    {
        int i = 0;
        foreach (Transform child in transform)
        {
            childOrigins[i] = child.position;
            childOrigins[i].y += 50;
            child.position = transform.position;
            i++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        int i = 0;
        foreach (Transform child in transform)
        {
            Vector2 dest;
            if (expand) dest = childOrigins[i];
            else dest = transform.position;
            child.position = Vector2.Lerp(child.position, dest, 10 * Time.unscaledDeltaTime);
            i++;
        }
    }

    public void ExpandChild()
    {
        if (!expand)
            StartCoroutine(Delay());
        else expand = false;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(10 * Time.unscaledDeltaTime);
        expand = !expand;
    }
}
