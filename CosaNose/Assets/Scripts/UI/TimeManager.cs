﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    private float timer;
    private int minutes = 0;
    private int hours = 0;
    private int day = 1;
    private int month = 1;
    private int year = 0;
    private int timeMultiplier = 1;
    private bool paused;

    [Tooltip("Divisor de 60. Segundos que dura una hora")]
    [SerializeField] int minutesAmount = 5;
    [SerializeField] int numHoursPerDay = 20;

    void Update()
    {
        timer += Time.deltaTime;

        TimeScale();
        UpdateDate();
    }

    private void TimeScale()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            GameSpeed();
        }

        if (Input.GetKeyDown(KeyCode.P)) PauseGame();

        if (paused) Time.timeScale = 0;
        else Time.timeScale = timeMultiplier;
    }

    public void GameSpeed()
    {
        timeMultiplier++;
        if (timeMultiplier > 3) timeMultiplier = 1;
    }

    public void PauseGame()
    {
        paused = !paused;
    }

    private void UpdateDate()
    {
        if (timer > 1)
        {
            minutes += 60 / minutesAmount;
            timer = 0;
        }
        if (minutes > 59)
        {
            minutes = 0;
            hours++;
        }
        if (hours > numHoursPerDay - 1)
        {
            hours = 0;
            day += 1;
            Debug.Log("New day");
        }
        if (day > 20)
        {
            day = 1;
            month++;
            Debug.Log("New month");
        }
        if (month > 6)
        {
            year++;
            month = 1;
            Debug.Log("New year");
        }
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(Screen.width * 0.9f, Screen.height * 0.97f, 1000, 100), hours + ":" + minutes + "   " + day + "/" + month + "/" + year);
    }

    public int GetDay() { return day; }
    public int GetHour() { return hours; }
    public int GetMinute() { return minutes; }
    public int GetMonth() { return month; }
    public int GetYear() { return year; }
    public int GetDurationOfDay() { return (60 / minutesAmount) * numHoursPerDay; }
}
