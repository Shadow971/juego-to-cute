﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUIElement : MonoBehaviour
{
    Vector2 startPos;
    Vector2 finalPos;
    Vector2 newPos;
    bool selected = false;
    void Start()
    {
        startPos = transform.position;
        newPos = transform.position;
        finalPos = startPos;
        finalPos.y += 50;
    }

    private void Update()
    {
        if (selected) newPos = finalPos;
        else newPos = startPos;
        transform.position = Vector2.Lerp(transform.position, newPos, 10 * Time.unscaledDeltaTime);
    }

    // Update is called once per frame
    public void MoveTab()
    {
        if (selected)
            StartCoroutine(Delay());
        else selected = true;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(10 * Time.unscaledDeltaTime);
        selected = !selected;
    }
}
