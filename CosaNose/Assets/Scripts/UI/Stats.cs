﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    [SerializeField] new string name;
    GameObject[] model;
    int modelIndex = 0;
    float growUp;
    float health;
    string date;
    string time;
    float timer = 0;
    bool grow = false;
    int generation;

    bool isGrounded = false;
    private Ray ray;
    private RaycastHit hit;

    void Awake()
    {
        model = new GameObject[transform.childCount];
        for(int i = 0; i < transform.childCount; i++)
        {
            model[i] = transform.GetChild(i).gameObject;
            if (i == 0) model[i].SetActive(true);
            else model[i].SetActive(false);
        }

        TimeManager t = FindObjectOfType<TimeManager>();
        date = t.GetDay() + "/" + t.GetMonth() + "/" + t.GetYear();
        time = t.GetHour() + ":" + t.GetMinute();
        health = 100;
        GenerateName n = new GenerateName();
        name = n.GetName();
    }
    public int GetGeneration() { return generation; }
    void SetGeneration(int g)  { generation = g; }

    private void Update()
    {
        ray = new Ray(transform.position, Vector3.down);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.distance < 0.2f)
            {
                isGrounded = true;
            }
            else
            {
                isGrounded = false;
            }
        }

        timer -= Time.deltaTime;
        if (timer < 0 && isGrounded)
        {
            timer = 1;
            health -= Random.Range(1,5);
            if(growUp < 100)
                growUp += 5;
        }

        if(growUp == 50 && !grow)
        {
            grow = true;
            model[modelIndex].SetActive(false);
            modelIndex++;
            model[modelIndex].SetActive(true);
        }

        if(health <= 0)
        {
            Vector3 v = transform.position;

            int rand = Random.Range(1, 3);
            generation++;
            for (int i = 0; i < rand; i++)
            {
                float randX = Random.Range(-2, 2);
                float randZ = Random.Range(-2, 2);
                int yAngle = Random.Range(1, 360);
                Vector3 newPos = v + new Vector3(randX, 0, randZ);
                GameObject go = Instantiate(gameObject, newPos, Quaternion.Euler(0, yAngle, 0));
                go.GetComponent<Stats>().SetGeneration(generation);
            }
            Destroy(gameObject);
        }
    }
    public void ChangeName(string name)
    {
        this.name = name;
        Debug.Log(name); 
    }
    public float GetHealth() { return health; }
    public string GetName() { return name; }
    public float GetGrowUp() { return growUp;  }
    public string GetDate() { return date; }
    public string GetTime() { return time; }
}
