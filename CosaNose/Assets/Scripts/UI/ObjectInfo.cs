﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectInfo : MonoBehaviour
{
    private GameObject target;
    [SerializeField] GameObject panel;
    [Range(-200, 200)]
    [SerializeField] int xOffset;
    [Range(-200, 200)]
    [SerializeField] int yOffset;

    bool xIn;
    bool yIn;

    private Ray ray;
    private RaycastHit hit;

    private void OnGUI()
    {   
        SetTarget();
        
        if (target != null)
        {
            Stats stats = target.GetComponent<Stats>();

            HealthBar(stats);
            Details(stats);
            MovePanel();

            panel.SetActive(true);
        }
        else
        {
            panel.SetActive(false);
        }
    }

    private void MovePanel()
    {
        Vector2 v = GetPanelPosition();
        Rect panelSize = panel.transform.GetComponent<RectTransform>().rect;

        if (Vector2.Distance(v, panel.transform.position) > Screen.width/4) target = null;


        if (v.x > 10 && v.x < Screen.width - 10 - panelSize.width)
            xIn = true;
        else
            xIn = false;

        if (v.y > 10 + panelSize.height && v.y < Screen.height - 10)
            yIn = true;
        else
            yIn = false;

        if (xIn && yIn)
        {
            panel.transform.position = v;
        }
        if (xIn && !yIn)
        {
            panel.transform.position = new Vector2(v.x, panel.transform.position.y);
        }
        if (!xIn && yIn)
        {
            panel.transform.position = new Vector2(panel.transform.position.x, v.y);
        }
    }

    private Vector3 GetPanelPosition()
    {
        Camera cam = Camera.main;
        Vector3 screenPos = cam.WorldToScreenPoint(target.transform.position);
        screenPos.x += xOffset;
        screenPos.y += yOffset;
        return screenPos;
    }

    private void Details(Stats stats)
    {
        Text name = panel.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>();
        name.text = "Name: " + stats.GetName();
        Text grow = panel.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<Text>();
        grow.text = "Grow Up: " + stats.GetGrowUp() + "%";
        Text day = panel.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<Text>();
        day.text = stats.GetDate();
        Text time = panel.transform.GetChild(0).GetChild(0).GetChild(4).GetComponent<Text>();
        time.text = stats.GetTime();
        Text generation = panel.transform.GetChild(0).GetChild(0).GetChild(6).GetComponent<Text>();
        generation.text = stats.GetGeneration().ToString();
    }

    private void HealthBar(Stats stats)
    {
        Slider health = panel.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Slider>();
        Image img = health.transform.GetChild(1).GetChild(0).GetComponent<Image>();
        float n = stats.GetHealth();

        if (n > 50)
        {
            img.color = Color.Lerp(img.color, Color.green, Time.deltaTime);
        }
        else if (n > 20 && n <= 50)
        {
            img.color = Color.Lerp(img.color, Color.yellow, Time.deltaTime);
        }
        else if (n <= 20)
        {
            img.color = Color.Lerp(img.color, Color.red, Time.deltaTime);
        }
        health.value = Mathf.Lerp(health.value, n, Time.deltaTime);
    }

    private void SetTarget()
    {
        Vector3 v = Input.mousePosition;
        v.z = 1;
        ray = Camera.main.ScreenPointToRay(v);
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.CompareTag("Movable"))
                {
                    target = hit.transform.gameObject;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape)) target = null;
    }

    public GameObject GetTarget() { return target; }
}
